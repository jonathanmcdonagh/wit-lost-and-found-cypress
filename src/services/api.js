import axios from 'axios'

export default() => {
  return axios.create({
    // baseURL: 'http://localhost:3000/'
    baseURL: 'https://wit-lost-and-found.herokuapp.com/'
    // baseURL: 'https://api-witlostandfound-staging.herokuapp.com/'
    // Front end Deployed on https://wit-lost-and-found.firebaseapp.com/#/
  })
}
